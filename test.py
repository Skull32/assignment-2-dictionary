import unittest
import subprocess

class Test(unittest.TestCase):
    def run_asm_app(self, input):
        pipe = subprocess.PIPE
        process = subprocess.Popen(
            "./app", stderr=pipe,
            stdin=pipe, stdout=pipe,
            shell=True, text=True
        )
        stdout, stderr = process.communicate(input=input)
        return (stdout.strip(), stderr.strip())
    
    def test_asm_app_key(self):
        self.assertEqual(
        self.run_asm_app("ii"),
        ("input value: fourth", "")
        )
        self.assertEqual(
        self.run_asm_app("third word"),
        ("input value: third", "")
        )
        self.assertEqual(
        self.run_asm_app("love"),
        ("input value: second", "")
        )
        self.assertEqual(
        self.run_asm_app("asm"),
        ("input value: first", "")
        )
        
    def test_asm_app_long(self):
        output_msg = ("input value:", "string length greater than 255")
        self.assertEqual(self.run_asm_app("A"*270), output_msg)
        
    def test_asm_app_not_found(self):
        output_msg = ("input value:", "value not found")
        self.assertEqual(self.run_asm_app("Apple"), output_msg)   

if __name__ == "__main__":
    unittest.main()