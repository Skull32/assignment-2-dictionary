ASM=nasm
ASMFLAGS=-f elf64
LD=ld
PYC3=python3

ASM_FILES=$(wildcard *.asm)
INC_FILES=$(wildcard *.inc)
OBJ_FILES=$(ASM_FILES:.asm=.o)

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm lib.inc

main.o: main.asm lib.inc dict.inc words.inc

.o: main.asm $(INC_FILES)	

app: $(OBJ_FILES) 
	$(LD) -o $@ $^

.PHONY: run clean test

run: app
	./app

clean:
	rm -rf *.o app

test: app
	$(PYC3) test.py
