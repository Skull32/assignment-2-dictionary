section .text
 
global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_string
global parse_uint
global parse_int
global string_copy
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi + rax], 0; сравниваем байт с 0
    je .end; если равно 0, то выходим
    inc rax; увеличиваем rax на 1
    jmp .loop; возвращаемся в начало цикла
.end:    
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi; кладем rdi на стек
    call string_length; вызываем функцию string_length
    mov rdx, rax; записываем rax в rdx
    pop rsi; забираем значение со стека и кладем в rsi
    mov rax, 1; номер системного вызова
    mov rdi, 1; дескриптор stdout
    syscall
    ret

print_error:
    push rdi; кладем rdi на стек
    call string_length; вызываем функцию string_length
    mov rdx, rax; записываем rax в rdx
    pop rsi; забираем значение со стека и кладем в rsi
    mov rax, 1; номер системного вызова
    mov rdi, 2; дескриптор stderr
    syscall
    ret    

; Принимает код символа и выводит его в stdout
print_char:
    push rdi; кладем rdi на стек
    mov rax, 1; номер системного вызова
    mov rdi, 1; дескриптор stdout
    mov rsi, rsp; записываем rsp в rsi
    mov rdx, 1; количество байт для записи
    syscall
    add rsp, 8
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`; записываем в rdi символ новой строки
    jmp print_char; переходим к print_char для вывода символа новой строки 

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi; записываем rdi в rax
    mov rcx, 10; записываем в rcx 10
    push word(0)
    mov r8, rsp
.loop:
    xor rdx, rdx; очищаем rdx
    div rcx; делим rax на 10
    dec r8; уменьшаем rsp на 1
    add dl, '0'; переводим число в ASCII
    mov byte[r8], dl; переносим символ в стек
    cmp rax, 0; проверяем что осталось при делении
    jnz .loop; если не равно 0 повторяем цикл
    mov rdi, r8;записываем адрес превого символа из rsp в rdi
    sub rsp, 24
    call print_string; вызываем функцию print_string
    add rsp, 26
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0; сравниваем число с 0
    jge print_uint; если больше 0 вызываем функцию print_unit
    neg rdi; инвертируем rdi
    push rdi; кладем на стек
    mov rdi, '-'; записываем знак минус
    call print_char; вызываем функцию print_char
    pop rdi; берем со стека занчения и кладем в rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
.loop:
    mov r8b, byte[rdi]; загружаем байт из rdi 
    cmp r8b, byte[rsi]; сравниваем с байтом из rsi
    jne .false; если не равны, то переходим к .false
    test r8b, r8b; проверим на конец строки
    jz .true; если равны, то переходим к .true
    inc rdi; переходим к следующему байту первой строки
    inc rsi; переходим к следующему байту второй строки
    jmp .loop; повторяем цикл
.true:
    mov rax, 1
    ret
.false:
    mov rax, 0
    ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push word(0); выделяем место на стеке
    xor rdi, rdi; устанавливаем файловый дискриптор на чтение
    mov rsi, rsp; кладем в rsi адрес резервированного места
    mov rdx, 1; устанавливаем количетсов байт
    syscall
    cmp rax, -1; сравниваем rax c -1
    jne .false; если не равны то переходим к .false
    add rsp, 2; освобождаем место
    ret 
.false:
    pop ax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx
    push r12
    push r13
    push r14
    mov r12, rcx
    mov r13, rdi
    mov r14, rsi
.loop:
    cmp r12, r14; cравниваем длину слова с зарезервированным местом
    jge .false; если больше или равно переходим .false
    call read_char; вызываем функцию read_char
    cmp rax, ` `; сравниваем символ с пробелом
    je .skip; если равен переходим к skip
    cmp rax, `\t`; сравниваем символ с символом табуляции
    je .skip; если равен переходим к skip
    cmp rax, `\n`; сравниваем символ с переводом строки
    je .skip; если равен переходим к skip
    mov [r13 + r12], rax; сохраняем символ 
    cmp rax, 0; проверяем на 0
    je .true; если равен переходим к true
    inc r12; увеличиваем rcx
    jmp .loop; повторяем цикл
.true:
    mov rax, r13; записываем в rax адрес начала слова
    mov rdx, r12; записываем в rdx длину слова
    pop r14
    pop r13
    pop r12
    ret
.false:
    pop r14
    pop r13
    pop r12
    xor rax, rax
    ret
.skip:
    test r12, r12
    jz .loop    
    jmp .true
 



read_string:
    xor rcx, rcx
    push r12
    push r13
    push r14
    mov r12, rcx
    mov r13, rdi
    mov r14, rsi
.loop:
    cmp r12, r14; cравниваем длину слова с зарезервированным местом
    jge .false; если больше или равно переходим .false
    call read_char; вызываем функцию read_char
    cmp rax, `\n`; сравниваем символ с переводом строки
    je .skip; если равен переходим к skip
    mov [r13 + r12], rax; сохраняем символ 
    cmp rax, 0; проверяем на 0
    je .true; если равен переходим к true
    inc r12; увеличиваем rcx
    jmp .loop; повторяем цикл
.true:
    mov rax, r13; записываем в rax адрес начала слова
    mov rdx, r12; записываем в rdx длину слова
    pop r14
    pop r13
    pop r12
    ret
.false:
    pop r14
    pop r13
    pop r12
    xor rax, rax
    ret
.skip:
    test r12, r12
    jz .loop    
    jmp .true 



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
xor r8, r8
xor rax, rax
xor rcx, rcx
.loop:
    mov r8b, byte[rdi+rcx]; загружаем число 
    cmp r8b, '0'; сравниваем с 0
    jl .end; если меньше переходим к end
    cmp r8b, '9'; сравниваем с 9
    jg .end; если меньше переходим к end
    imul rax, 10; умножаем на 10
    sub r8b, '0'; преобразуем в число
    add rax, r8; сдвигаем разряд
    inc rcx; увеличиваем длину числа
    jmp .loop; повтор цикла
.end:
    mov rdx, rcx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'; проверяем является ли первый символ -
    je .neg; если нет, то переходим к neg
    cmp byte[rdi], '+'; проверяем является ли первый символ +
    je .pos; если нет, то переходим к pos   
    jmp parse_uint; вызываем функцию parse_uint
.neg: 
    inc rdi; увеличиваем rdi
    call parse_uint
    cmp rdx, 0
    je .end
    inc rdx; увеличиваем rdx
    neg rax; инвертируем значение 
    ret
.pos:
    inc rdi; увеличиваем rdi
    call parse_uint
    cmp rdx, 0
    je .end
    inc rdx; увеличиваем rdx
.end:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
xor rax, rax
.loop:
    cmp rax, rdx; сравниваем длину строки и счетчик
    jge .false; если больше или равно переходим к false
    mov r8b, byte[rdi+rax]; загружаем байт из исходной строки
    mov byte[rsi+rax], r8b; сохраняем его
    cmp r8b, 0; сравниваем с 0
    je .true; если равно то переходим к true
    inc rax; увеличиваем rax на 1
    jmp .loop; повторяем цикл
.false:
    xor rax, rax
.true:
    ret
