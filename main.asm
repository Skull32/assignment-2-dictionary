%include "words.inc"
%include "lib.inc"
%include "dict.inc"

%define MAX_SIZE 255
%defstr MAX_SIZE_STR MAX_SIZE

section .data
    start_msg: db 'input value: ', 0
    not_found_msg: db 'value not found', `\n`, 0
    length_error_msg: db "string length greater than ", MAX_SIZE_STR, `\n`, 0

section .bss
input_buffer: resb MAX_SIZE + 1

section .text
global _start

get_value:
    push rdi
    call string_length
    pop rdi     
    add rdi, rax            
    inc rdi                           
    ret

_start:
    mov rdi, start_msg
    call print_string
    mov rsi, MAX_SIZE
    mov rdi, input_buffer
    call read_string
    test rax, rax
    je .false
    mov rsi, first_word
    mov rdi, rax
    call find_word
    test rax, rax
    jnz .true
    mov rdi, not_found_msg
    jmp .end
    .true:
        mov rdi, rax
        call get_value
        call print_string
        call print_newline
        xor rdi, rdi
        call exit
    .false:
        mov rdi, length_error_msg
    .end:
        call print_error
        mov rdi, 1
        jmp exit
