%include "lib.inc"

%define SIZE 8
section .text

global find_word

find_word:
    .loop:
        add rsi, SIZE
        push rsi
        call string_equals
        pop rsi
        test rax, rax
        jnz .true
        sub rsi, SIZE
        mov rsi, [rsi]
        test rsi, rsi
        jz .false
        jmp .loop
    .false:
        xor rax,rax
        jmp .end
    .true:
        mov rax, rsi
    .end:
        ret
